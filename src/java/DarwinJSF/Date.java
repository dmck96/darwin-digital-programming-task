package DarwinJSF;

import java.time.LocalDate;
import java.time.Month;

public class Date {
	private Month month;
	private String payDay;
	private String bonusDay;
	
	// Defining object
	public Date(Month month, String payDay, String bonusDay) {
		this.month = month;
		this. payDay = payDay;
		this. bonusDay = bonusDay;
	}

    /**
     * @return the month
     */
    public Month getMonth() {
        return month;
    }

    /**
     * @return the payDay
     */
    public String getPayDay() {
        return payDay;
    }

    /**
     * @return the bonusDay
     */
    public String getBonusDay() {
        return bonusDay;
    }
}
