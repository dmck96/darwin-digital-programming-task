package DarwinJSF;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Daniel Mckibbin
 */
@Named(value = "theMainBean")
@SessionScoped
	
public class main implements Serializable {
    
    private static DateTimeFormatter parser = DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy");
    private static ArrayList<Date> dateList = new ArrayList<Date>();
        @PostConstruct
    	public static void main(){
	    for(int i = 0; i < 12; i++) {
			LocalDate today = LocalDate.now().plusMonths(i);
			LocalDate bonus = today.withDayOfMonth(15);
			LocalDate end = today.withDayOfMonth(today.lengthOfMonth());
			DayOfWeek dayOfWeekP = end.getDayOfWeek();
			DayOfWeek dayOfWeekB = bonus.getDayOfWeek();
			Month month = today.getMonth();
			
			if(dayOfWeekP == DayOfWeek.SATURDAY) {
				end = end.minusDays(1);
			}
			if(dayOfWeekP == DayOfWeek.SUNDAY) {
				end = end.minusDays(2);
			}
			
			if(dayOfWeekB == DayOfWeek.SATURDAY) {
				bonus = bonus.plusDays(4);
			}
			if(dayOfWeekB == DayOfWeek.SUNDAY) {
				bonus = bonus.plusDays(3);
			}
                        String endParsed = end.format(parser);
                        String bonusParsed = bonus.format(parser);
			
			dateList.add(new Date(month, endParsed, bonusParsed));
	     }
	}
        
    /**
     * @return the dateList
     */
    public static ArrayList<Date> getDateList() {
        return dateList;
    }
}
